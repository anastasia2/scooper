package main

import (
	"log"
	"os"
	"strings"

	"github.com/jlubawy/go-boilerpipe/normurl"
	"github.com/thatguystone/swan"
)

func main() {
	u, err := normurl.Parse(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	a, err := swan.FromURL(u.String())
	if err != nil {
		log.Fatal(err)
	}
	if a.TopNode == nil {
		panic("no article could be extracted from " +
			u.String() +
			"but a.Doc and a.Meta are still cleaned " +
			"and can be messed with ")
	}
	filename := strings.Replace(u.String(), "/", "-", -1)
	if a.Meta.Title != "" {
		filename = strings.Replace(a.Meta.Title, " ", "-", -1)
	}
	f, err := os.OpenFile("/home/nastia/"+filename+"-cleaned.md", os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	if a.Meta.Title == "" {
		f.WriteString("# Untitled")
	} else {
		f.WriteString("# " + a.Meta.Title)
	}
	f.WriteString("\n\nURL: <" + u.String() + ">\n\n")
	if (a.Meta.OpenGraph["site_name"] != "") || (a.Meta.PublishDate != "") || (len(a.Meta.Authors) > 0) || (a.Meta.Domain != "") || (a.Meta.Keywords != "") || (a.Meta.OpenGraph["description"] != "") || (a.Meta.Description != "") {
		f.WriteString("## Metadata\n\n")

		if a.Meta.OpenGraph["site_name"] != "" {
			f.WriteString("Site Name: " + a.Meta.OpenGraph["site_name"] + "\n")
		} else if a.Meta.Domain != "" {
			f.WriteString("Domain: " + a.Meta.Domain + "\n")
		}
		if a.Meta.Keywords != "" {
			f.WriteString("Keywords: " + a.Meta.Keywords + "\n")
		}
		if a.Meta.OpenGraph["description"] != "" {
			f.WriteString("Description: " + a.Meta.OpenGraph["description"] + "\n")
		} else if a.Meta.Description != "" {
			f.WriteString("Description: " + a.Meta.Description + "\n")
		}
		if a.Meta.PublishDate != "" {
			f.WriteString("Date: " + a.Meta.PublishDate + "\n")
		}
		if len(a.Meta.Authors) > 0 {
			f.WriteString("Author(s)")
			for author := range a.Meta.Authors {
				f.WriteString(", " + a.Meta.Authors[author])
			}
			f.WriteString("\n")
		}
		f.WriteString("\n")
	}
	if (len(os.Args) > 2) && (a.CleanedText != "") {
		f.WriteString("## Article Text\n\n")
		if a.Img != nil {
			f.WriteString("Header Image URL: <" + a.Img.Src + ">\n\n")
		}
		f.WriteString(a.CleanedText + "\n\n")
	}
	if len(a.Meta.Links) > 0 {
		f.WriteString("## Links\n")
		for link := range a.Meta.Links {
			f.WriteString("\n<" + a.Meta.Links[link] + ">")
		}
	}

}
