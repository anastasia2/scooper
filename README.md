# Scooper

uses [swan](https://github.com/thatguystone/swan) and [go-boilerpipe](https://github.com/jlubawy/go-boilerpipe) to write metadata & text from an article to a markdown file.
